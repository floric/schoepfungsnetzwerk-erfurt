import { defineConfig } from "astro/config";
import tailwind from "@astrojs/tailwind";
import sitemap from "@astrojs/sitemap";

// https://astro.build/config
export default defineConfig({
  integrations: [
    sitemap({
      changefreq: "weekly",
      priority: 0.7,
      lastmod: new Date(),
    }),
    tailwind({ applyBaseStyles: false }),
  ],
  output: "static",
  trailingSlash: "always",
  site: "https://schoepfungsnetzwerk-erfurt.de",
  prefetch: true,
  cacheDir: ".astro-cache",
  experimental: {
    clientPrerender: true,
  },
  prefetch: {
    prefetchAll: true,
  },
});
