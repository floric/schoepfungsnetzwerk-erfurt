import type { CollectionEntry } from "astro:content";

export function buildNewsSlug(entry: CollectionEntry<"news">) {
  const date = entry.data.pubDate
    .toLocaleDateString("sv-SE" /* == ISO */, {
      month: "2-digit",
      day: "2-digit",
      year: "numeric",
    })
    .replaceAll(".", "-");
  return `${date}-${entry.slug}`;
}
