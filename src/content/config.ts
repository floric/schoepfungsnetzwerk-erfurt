import { z, defineCollection, reference } from "astro:content";

const newsCollection = defineCollection({
  type: "content",
  schema: z.object({
    title: z.string(),
    summary: z.string().max(300),
    tags: z.array(z.string()).default([]),
    pubDate: z.date(),
    events: z.array(reference("events")).default([]),
  }),
});

const eventsCollection = defineCollection({
  type: "data",
  schema: z.object({
    title: z.string(),
    date: z.date(),
    time: z.string().optional(),
    summary: z.string().max(1000).optional(),
    location: z.string().max(500).optional(),
    links: z
      .array(
        z.object({
          name: z.string(),
          url: z.string(),
        }),
      )
      .default([]),
  }),
});

export const collections = {
  news: newsCollection,
  events: eventsCollection,
};
