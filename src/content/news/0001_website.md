---
title: Webseite veröffentlicht
tags:
  - Online
slug: website-veroeffentlicht
pubDate: 2023-12-23
summary: Seit einigen Tagen ist diese Webseite online, um über unser Netzwerk zu informieren. Einzelne Inhalte sind allerdings noch im Aufbau.
---

Seit einigen Tagen ist diese Webseite online, um über unser Netzwerk zu informieren. Wir wollen uns vorstellen und dich einladen, dich mit uns gemeinsam für nachhaltiges Handeln in unseren Kirchengemeinden und unserem Umfeld zu engagieren.

Einzelne Inhalte wie Details zu unseren Zielen sind allerdings noch im Aufbau. Diese werden wir die nächsten Tage ausführlicher beschreiben. Bitte habe da noch ein wenig Geduld und schau gerne in ein paar Tagen wieder vorbei.
