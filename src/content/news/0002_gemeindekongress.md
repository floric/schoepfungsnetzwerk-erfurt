---
title: Aussteller beim Gemeindekongress
tags:
  - Gemeindekongress
slug: aussteller-gemeindekongress
pubDate: 2025-02-27
summary: Das Schöpfungsnetzwerk war Aussteller beim "Markt der sprühenden Ideen" beim Gemeindekongress am 22.02.2025.
---

Beim Gemeindekongress der EKM am 22.02.2025 präsentierte sich das Schöpfungsnetzwerk auf dem "Markt der sprühenden Ideen". Um den Vernetzungsgedanken anschaulich zu machen, spannte sich ein dichtes Netz aus Verbindungen über eine Karte von Erfurt mit allen Orten, an denen wir bereits aktiv sind oder waren. Besucher konnten sich eine "Hausaufgabe" mitnehmen, die sie in ihrer Gemeinde umsetzen möchten. Damit möchten wir Nachhaltigkeitsideen weiter in die Gemeinden tragen. Unser Stand stieß auf reges Interesse - schade nur, dass es nach 2,5 Stunden schon wieder zu Ende war.
[![Stand](/images/2025-02-22-Stand.jpg "Unser Stand beim Gemeindekongress")](/images/2025-02-22-Stand.jpg)

|  |  |
| --- | --- |
| [![Umfrage](/images/2025-02-22-Umfrage.jpg "Ist Schöpfungsbewahrung ein zentraler Auftrag an Kirche?")](/images/2025-02-22-Umfrage.jpg) | [![Hausaufgaben](/images/2025-02-22-Hausaufgaben.jpg "Aufgaben zum Mitnehmen")](/images/2025-02-22-Hausaufgaben.jpg) |
| [![Netzwerk](/images/2025-02-22-Netzwerk.jpg "Ein Netzwerk aus Erfurter Gemeinden")](/images/2025-02-22-Netzwerk.jpg) | [![Drei Standbeine](/images/2025-02-22-Dreibein.jpg "Drei Standbeine")](/images/2025-02-22-Dreibein.jpg) |