Die Erde ist unsere Heimat, ein kostbarer und einmaliger Lebensraum. Wir sind auf sie angewiesen, wir sind ein Teil der Schöpfung.

Und doch gefährden wir diesen Lebensraum durch Maßlosigkeit und ungebremsten Verbrauch, durch ungerechte Verteilung, durch Trägheit und Unachtsamkeit. In der Bibel (Gen. 2,15) steht: _„Gott der Herr nahm den Menschen und setzte ihn in den Garten Eden, dass er ihn bebaute und bewahrte.“_

Nie war das Bewahren so aktuell wie heute. Wie werden wir diesem Anliegen und Auftrag ganz konkret und im Alltag gerecht?
